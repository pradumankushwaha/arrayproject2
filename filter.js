function filter(elements=[], cb) {
    if(elements.length==0){
        return []
    }
    let newArray=[]
    for(index=0;index<elements.length;index++){
        //let item=elements[index]
        if(cb(elements[index],index,elements)==true)
        {
            newArray.push(elements[index])

        }
    }
    return newArray;
}
module.exports=filter;