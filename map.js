function map(elements, cb) {
    if(elements.length===0)
    {
      return[];
    }
    let arr=[];
    for(index=0;index<elements.length;index++)
    {
      let output=cb(elements[index],index,elements);
      arr.push(output);
    }
    return arr;
  }
  module.exports=map;